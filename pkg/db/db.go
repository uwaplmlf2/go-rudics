// Package db provides the RUDICS CouchDB database interface
package db

import (
	"encoding/json"
	"time"
)

// CouchDB database entry for a device
type Device struct {
	ID       string `json:"_id"`
	Rev      string `json:"_rev,omitempty"`
	Name     string `json:"name"`
	Imei     string `json:"imei"`
	Type     string `json:"type"`
	Owner    string `json:"owner,omitempty"`
	Endpoint string `json:"endpoint,omitempty"`
}

type Time struct {
	time.Time
}

// UnmarshalJSON implements the json.Unmarshal interface, it unmarshals a new
// Time value from integer seconds since the Unix Epoch.
func (t *Time) UnmarshalJSON(b []byte) error {
	var tsecs int64
	if err := json.Unmarshal(b, &tsecs); err != nil {
		return err
	}
	if tsecs == 0 {
		*t = Time{}
	} else {
		t.Time = time.Unix(tsecs, 0).UTC()
	}
	return nil
}

// MarshalJSON implements the json.Marshal interface, it marshals a Time value
// to integer seconds since the Unix Epoch.
func (t Time) MarshalJSON() ([]byte, error) {
	var tsecs int64
	if t.Time.IsZero() {
		tsecs = 0
	} else {
		tsecs = t.Time.Unix()
	}
	return json.Marshal(tsecs)
}

type Session struct {
	ID       string `json:"_id,omitempty"`
	Rev      string `json:"_rev,omitempty"`
	Imei     string `json:"imei"`
	Tconnect Time   `json:"connect"`
	Tlogin   Time   `json:"login"`
	Tlogout  Time   `json:"logout"`
	In       int64  `json:"bytes_in"`
	Out      int64  `json:"bytes_out"`
	Type     string `json:"type"`
}

func (s Session) String() string {
	b, _ := json.Marshal(s)
	return string(b)
}
