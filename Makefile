
SVCDIR = /etc/systemd/system
# If the following two variables are changed, be sure to update
# the systemd .service file
BINDIR = /usr/local/bin
CFGDIR = /usr/local/etc

APPS := rudicsgw rudicsdb
UNIT = rudicsgw.socket
SVCFILE = cmd/rudicsgw/rudicsgw.service cmd/rudicsgw/rudicsgw.socket
CFGFILE = cmd/rudicsgw/config.toml

install-bin: $(APPS)
	install -m 755 -t $(BINDIR) $(APPS)

install: install-bin $(SVCFILE) $(CFGFILE)
	install -d $(BINDIR) $(CFGDIR) $(SVCDIR)
	@cp -v $(SVCFILE) $(SVCDIR)
	@cp -v $(CFGFILE) $(CFGDIR)/rudicsgw.toml
	@chmod 600 $(CFGDIR)/rudicsgw.toml

update: install-bin
	systemctl restart $(UNIT)

uninstall:
	systemctl disable $(UNIT)
	systemctl stop $(UNIT)
	rm -f $(BINDIR)/rudicsgw $(BINDIR)/rudicsdb
	rm -f $(CFGDIR)/rudicsgw.toml
