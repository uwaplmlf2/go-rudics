module bitbucket.org/uwaplmlf2/go-rudics

go 1.13

require (
	bitbucket.org/uwaplmlf2/couchdb v0.7.1
	github.com/BurntSushi/toml v0.3.1
	github.com/coreos/go-systemd/v21 v21.0.0-20191108165717-5a0db84d3dc4
	github.com/imdario/mergo v0.3.8
	github.com/jpillora/ipfilter v1.2.2
	github.com/kr/pty v1.1.8
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/urfave/cli v1.22.1
	golang.org/x/crypto v0.0.0-20191106202628-ed6320f186d4
)
