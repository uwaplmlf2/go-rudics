// Rudicsdb provides a CLI for managing the device entries in the database
// used by the RUDICS gateway.
package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"net/url"
	"os"
	"os/signal"
	"strings"

	"bitbucket.org/uwaplmlf2/couchdb"
	"bitbucket.org/uwaplmlf2/go-rudics/pkg/db"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
	"golang.org/x/crypto/ssh/terminal"
)

const (
	VIEW   = "rudics/devices"
	FIELDS = 4
)

var Version = "dev"
var BuildDate = "unknown"

func dumpDb(rdb *couchdb.Database, w io.Writer) error {
	wtr := csv.NewWriter(w)
	header := []string{"imei", "owner", "endpoint", "name"}

	rows, err := rdb.Query(couchdb.NewView(VIEW), nil)
	if err != nil {
		return errors.Wrap(err, "query")
	}

	wtr.Write(header)
	rec := make([]string, 4)
	for rows.Next() {
		dev := db.Device{}
		if err := rows.ScanValue(&dev); err != nil {
			return errors.Wrap(err, "decode value")
		}
		rec[0] = dev.Imei
		rec[1] = dev.Owner
		rec[2] = dev.Endpoint
		rec[3] = dev.Name
		wtr.Write(rec)
	}
	wtr.Flush()
	return wtr.Error()
}

func updateDb(rdb *couchdb.Database, r io.Reader) error {
	var dev db.Device
	rdr := csv.NewReader(r)
	line := int(1)
	for {
		rec, err := rdr.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("Line %d: Read error: %w", line, err)
		}

		if len(rec) != FIELDS {
			return fmt.Errorf("Line %d: Bad input format", line)
		}

		rdb.Get(rec[0], &dev)

		dev.Imei = rec[0]
		dev.Owner = rec[1]
		dev.Endpoint = rec[2]
		dev.Name = rec[3]
		dev.ID = dev.Imei

		if dev.Endpoint == "" {
			dev.Type = "local"
		} else {
			dev.Type = "proxy"
		}

		_, err = rdb.Put(dev.ID, dev)
		if err != nil {
			return fmt.Errorf("Cannot update entry %q: %w", dev.ID, err)
		}
		line++
	}

	return nil
}

func deleteEntry(rdb *couchdb.Database, imei string) error {
	_, err := rdb.Delete(imei, "")
	return err
}

func getPassword(prompt string) ([]byte, error) {
	fd := int(os.Stdin.Fd())
	state, err := terminal.GetState(fd)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	defer signal.Stop(ch)

	go func() {
		_, ok := <-ch
		if ok {
			terminal.Restore(fd, state)
			os.Exit(1)
		}
	}()

	fmt.Print(prompt)
	pass, err := terminal.ReadPassword(fd)
	fmt.Println("")

	return pass, err
}

func main() {
	app := cli.NewApp()
	app.Name = "rudicsdb"
	app.Usage = "Manage the database for the RUDICS gateway"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "addr",
			Usage:  "CouchDB host:port",
			Value:  "50.18.188.61:5984",
			EnvVar: "RUDICS_DB_HOST",
		},
		cli.StringFlag{
			Name:   "db",
			Usage:  "Database name",
			Value:  "rudics",
			EnvVar: "RUDICS_DB_NAME",
		},
		cli.StringFlag{
			Name:   "user, u",
			Usage:  "username:password",
			EnvVar: "RUDICS_DB_USER",
		},
		cli.VersionFlag,
	}

	var (
		err error
		rdb *couchdb.Database
	)

	app.Before = func(c *cli.Context) error {
		u := url.URL{}
		u.Scheme = "http"
		u.Host = c.String("addr")

		if user := c.String("user"); user != "" {
			creds := strings.Split(user, ":")
			switch len(creds) {
			case 1:
				pass, err := getPassword("Database password: ")
				if err != nil {
					return err
				}
				u.User = url.UserPassword(creds[0], string(pass))
			case 2:
				u.User = url.UserPassword(creds[0], creds[1])
			}
		}
		rdb = couchdb.NewDatabase(u.String(), c.String("db"))
		return err
	}

	app.Commands = []cli.Command{
		{
			Name:  "dump",
			Usage: "dump RUDICS device database",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "output, o",
					Usage: "write output to FILE",
					Value: "",
				},
			},
			Action: func(c *cli.Context) error {
				var f io.Writer
				if c.String("output") != "" {
					f, err = os.Create(c.String("output"))
					if err != nil {
						return cli.NewExitError(
							errors.Wrap(err, "open failed"), 1)
					}
				} else {
					f = os.Stdout
				}
				err := dumpDb(rdb, f)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				return nil
			},
		},
		{
			Name:  "update",
			Usage: "update entries in the RUDICS device database",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "input, i",
					Usage: "read input from FILE",
					Value: "",
				},
			},
			Action: func(c *cli.Context) error {
				var f io.Reader
				if c.String("input") != "" {
					f, err = os.Open(c.String("input"))
					if err != nil {
						return cli.NewExitError(
							errors.Wrap(err, "open failed"), 1)
					}
				} else {
					f = os.Stdin
				}
				err := updateDb(rdb, f)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				return nil
			},
		},
		{
			Name:      "delete",
			Usage:     "delete one or more entries from the RUDICS device database",
			ArgsUsage: "IMEI [IMEI ...]",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}

				for _, imei := range c.Args() {
					if err := deleteEntry(rdb, imei); err != nil {
						return cli.NewExitError(err, 1)
					}
				}
				return nil
			},
		},
	}

	app.Run(os.Args)
}
