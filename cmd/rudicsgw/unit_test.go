package main

import "testing"

func TestIPBlock(t *testing.T) {
	table := []struct {
		cfg     accessConfig
		addr    string
		allowed bool
	}{
		{
			cfg:     accessConfig{},
			addr:    "1.2.3.4",
			allowed: true,
		},
		{
			cfg:     accessConfig{Allow: []string{"10.95.97.0/24", "127.0.0.1", "12.47.179.0/24", "192.108.98.20"}, Deny: []string{"0.0.0.0/0"}},
			addr:    "1.2.3.4",
			allowed: false,
		},
		{
			cfg:     accessConfig{Allow: []string{"10.95.97.0/24", "127.0.0.1", "12.47.179.0/24", "192.108.98.20"}, Deny: []string{"0.0.0.0/0"}},
			addr:    "192.108.98.20",
			allowed: true,
		},
	}

	for _, e := range table {
		filter, err := createIPFilter(e.cfg)
		if err != nil {
			t.Fatal(err)
		}
		result := filter.Allowed(e.addr)
		if result != e.allowed {
			t.Errorf("Unexpected result for %q; got %v, expected %v",
				e.addr, result, e.allowed)
		}
	}
}
