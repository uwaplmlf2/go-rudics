// Rudicsgw implements a TCP server that provides an endpoint for Iridium
// modems using a RUDICS gateway. This server is essentially a second
// level gateway that routes connections based on the mobile device's
// Iridium Serial Number (IRSN) aka IMEI.
package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"strings"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/uwaplmlf2/couchdb"
	"bitbucket.org/uwaplmlf2/go-rudics/pkg/db"
	"github.com/BurntSushi/toml"
	"github.com/coreos/go-systemd/v21/activation"
	"github.com/imdario/mergo"
	"github.com/jpillora/ipfilter"
	"github.com/kr/pty"
	"github.com/pkg/errors"
)

type dbConfig struct {
	User     string `toml:"user"`
	Password string `toml:"password"`
	Host     string `toml:"host"`
	Dbname   string `toml:"dbname"`
}

type svrConfig struct {
	Address string `toml:"address"`
	ImageId string `toml:"image_id"`
	Tlogin  int64  `toml:"login_timeout"`
}

type accessConfig struct {
	Allow []string `toml:"allow"`
	Deny  []string `toml:"deny"`
}

type sysConfig struct {
	Db     dbConfig     `toml:"database"`
	Svr    svrConfig    `toml:"server"`
	Access accessConfig `toml:"access"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
rudicsgw [options] configfile

Iridium RUDICS gateway service.
`

var DEFAULT = `
[database]
host = "50.18.188.61:5984"
user = ""
password = ""
dbname = "rudics"
[server]
address = ":10130"
image_id = "mfk/mlf2host:latest"
login_timeout = 30
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	cfgfile = flag.String("cfgfile", "",
		"Path to the configuration file")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
)

func (cfg dbConfig) Uri() string {
	return fmt.Sprintf("http://%s:%s@%s/",
		cfg.User,
		cfg.Password,
		cfg.Host)
}

func createIPFilter(cfg accessConfig) (*ipfilter.IPFilter, error) {
	opts := ipfilter.Options{
		IPDBNoFetch: false,
	}

	for _, ip := range cfg.Deny {
		if ip == "0.0.0.0/0" {
			opts.BlockByDefault = true
		}
	}

	f := ipfilter.New(opts)

	if !opts.BlockByDefault {
		for _, ip := range cfg.Deny {
			f.BlockIP(ip)
		}
	}

	for _, ip := range cfg.Allow {
		f.AllowIP(ip)
	}

	return f, nil
}

func readUntil(rdr io.Reader, c byte) ([]byte, error) {
	var (
		buf bytes.Buffer
		err error
		n   int
	)

	b := make([]byte, 1)
	for {
		n, err = rdr.Read(b)
		if n > 0 {
			buf.Write(b[0:n])
		}
		if err != nil || c == b[0] {
			break
		}
	}

	return buf.Bytes(), err
}

func getDevice(ctx context.Context, conn net.Conn, rdb *couchdb.Database) (db.Device, error) {
	var dev db.Device

	conn.Write([]byte("irsn: "))
	resp, err := readUntil(conn, '\n')
	if err != nil {
		return dev, errors.Wrap(err, "login failed")
	}
	imei := string(bytes.Trim(resp, "\r\n \t\x00\xff"))
	log.Printf("Login; IMEI=%s", imei)
	if imei == "" {
		return dev, errors.New("Blank IMEI")
	}

	err = rdb.Get(imei, &dev)
	return dev, err
}

func stopContainer(name string) {
	cmd := exec.Command("/usr/bin/docker", "stop", name)
	_ = cmd.Run()
}

func handleConnection(ctx context.Context, conn net.Conn, rdb *couchdb.Database,
	image_id string) {
	s := db.Session{
		Type:     "session",
		Tconnect: db.Time{time.Now().UTC()},
	}

	dev, err := getDevice(ctx, conn, rdb)
	if err != nil {
		log.Println(err)
		return
	}

	var t time.Time
	conn.SetReadDeadline(t)
	s.Tlogin = db.Time{time.Now().UTC()}
	s.Imei = dev.Imei

	var (
		w io.WriteCloser
		r io.ReadCloser
	)

	var name string
	if dev.Type == "proxy" {
		// Open a connection to the endpoint associated with this device
		rconn, err := net.Dial("tcp", dev.Endpoint)
		if err != nil {
			log.Printf("Connection to %q failed: %v", dev.Endpoint, err)
			return
		}
		w = rconn
		r = rconn
		defer rconn.Close()
	} else {
		// Start a Docker container to handle an MLF2 device.
		// Name == mlf2-N -> "mlf2", "N"
		parts := strings.Split(dev.Name, "-")
		name = fmt.Sprintf("mlf2-%s-%d", parts[1], time.Now().Unix())
		args := []string{
			"run",
			"-it",
			"--rm",
			"--name", name,
			"-v", "/var/log/mlf2:/var/log/mlf2",
			"-e", "TERM=vt100",
			"-e", "IRSN=" + s.Imei,
			"-e", "FLOATID=" + parts[1],
		}
		if val, ok := os.LookupEnv("AMQP_SERVER"); ok {
			args = append(args, "-e")
			args = append(args, "AMQP_SERVER="+val)
		}

		if val, ok := os.LookupEnv("DB_SERVER"); ok {
			args = append(args, "-e")
			args = append(args, "DB_SERVER="+val)
		}
		args = append(args, image_id)

		cmd := exec.CommandContext(ctx, "/usr/bin/docker", args...)

		f, err := pty.Start(cmd)
		if err != nil {
			log.Printf("Cannot start Docker: %v", err)
			return
		}
		defer f.Close()
		w = f
		r = f
	}

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		defer w.Close()
		defer conn.Close()
		var err error
		s.In, err = io.Copy(w, conn)
		log.Printf("Copy-in returns: (%d, %v)", s.In, err)
		if name != "" {
			go stopContainer(name)
		}
	}()
	go func() {
		defer wg.Done()
		defer r.Close()
		defer conn.Close()
		var err error
		s.Out, err = io.Copy(conn, r)
		log.Printf("Copy-out returns: (%d, %v)", s.Out, err)
	}()
	wg.Wait()

	s.Tlogout = db.Time{time.Now().UTC()}
	s.ID = fmt.Sprintf("%s_%08x", s.Imei, s.Tconnect.Unix())
	log.Printf("%s\n", s)
	_, err = rdb.Put(s.ID, s)
	if err != nil {
		log.Printf("Cannot log session: %v", err)
	}
}

func rudicsServe(ctx context.Context, listener net.Listener,
	cfg sysConfig,
	rdb *couchdb.Database,
	ipf *ipfilter.IPFilter) error {
	login_timeout := time.Duration(cfg.Svr.Tlogin) * time.Second
	for {
		c, err := listener.Accept()
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		if err != nil {
			return err
		}

		if ipf != nil {
			addr := c.RemoteAddr().String()
			if n := strings.LastIndex(addr, ":"); n != -1 {
				if !ipf.Allowed(addr[:n]) {
					log.Printf("Access denied from %q", addr)
					c.Close()
					continue
				}
			}
		}

		c.SetReadDeadline(time.Now().Add(login_timeout))
		go handleConnection(ctx, c, rdb, cfg.Svr.ImageId)
	}
}

func loadConfiguration(base, cfgfile string, cfg *sysConfig) error {
	var (
		defcfg sysConfig
		err    error
	)

	if err = toml.Unmarshal([]byte(base), &defcfg); err != nil {
		return errors.Wrap(err, "default config")
	}

	if cfgfile != "" {
		b, err := ioutil.ReadFile(cfgfile)
		if err != nil {
			return errors.Wrap(err, "read config file")
		}

		if err = toml.Unmarshal(b, cfg); err != nil {
			return errors.Wrap(err, "parse config file")
		}
	}

	mergo.Merge(cfg, defcfg)

	return nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Printf("%s", DEFAULT)
		os.Exit(0)
	}

	var cfg sysConfig
	if err := loadConfiguration(DEFAULT, *cfgfile, &cfg); err != nil {
		log.Fatal(err)
	}

	rdb := couchdb.NewDatabase(cfg.Db.Uri(), cfg.Db.Dbname)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ipf, _ := createIPFilter(cfg.Access)

	listeners, err := activation.Listeners()
	if err != nil {
		log.Fatalf("Systemd activation error: %v", err)
	}

	var listener net.Listener
	switch n := len(listeners); n {
	case 0:
		// Not activated by a Systemd socket ...
		addr, _ := net.ResolveTCPAddr("tcp", cfg.Svr.Address)
		listener, err = net.ListenTCP("tcp", addr)
		if err != nil {
			log.Fatal(err)
		}
	case 1:
		listener = listeners[0]
	default:
		log.Fatalf("Unexpected number of socket activation fds: %d", n)
	}

	// Cancel the context on a signal interrupt
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
			listener.Close()
		}
	}()

	log.Printf("RUDICS gateway (%s), Waiting for connections", Version)

	if err = rudicsServe(ctx, listener, cfg, rdb, ipf); err != nil {
		log.Fatal(err)
	}
}
