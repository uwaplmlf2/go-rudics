# RUDICS Gateway

## Introduction

[RUDICS](http://www.nalresearch.com/NetRef_RUDICS.html) is a service
provided by [Iridium](http://www.iridium.com/) to allow data connections
from a mobile device to a TCP server. This server provides an endpoint for
that service.

The server differentiates connections using the mobile device's Iridium
Serial Number (IRSN, aka IMEI). Connections from MLF2 floats are provided
with a shell log-in to a Docker container while others are provided a
transparent connenction to a separate TCP server (referred to in this
document as the downstream server).
